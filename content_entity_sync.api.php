<?php

/**
 * @file
 * Hooks provided by the Content Entity Sync module.
 */

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Alters the list of base config dependencies.
 *
 * This hook can notably be used to change the label of the empty option.
 *
 * @param array $options
 *   The array of options for the field, as returned by
 *   \Drupal\Core\TypedData\OptionsProviderInterface::getSettableOptions(). An
 *   empty option (_none) might have been added, depending on the field
 *   properties.
 * @param \Drupal\Core\Entity\ContentEntityInterface $entity
 *
 * @ingroup hooks
 * @see hook_options_list()
 */
function hook_base_config_dependencies_alter(array &$dependencies, ContentEntityInterface $entity) {
  // Check if this is the field we want to change.
  if ($context['fieldDefinition']->getName() == 'field_option') {
    // Change the label of the empty option.
    $options['_none'] = t('== Empty ==');
  }
}

function hook_entity_serializer_alter() {
  
}
