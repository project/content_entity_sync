<?php

namespace Drupal\content_entity_sync_ui;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class LocalTasksAlterHandler {

  use StringTranslationTrait;

  /**
   * Constructs a new EetHooks.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeBundleInfoInterface $bundleInfo,
  ) {
  }

  /**
   * Generates all bundles in the system as a flat series of arrays.
   */
  protected function bundles() {
    foreach ($this->bundleInfo->getAllBundleInfo() as $entityTypeId => $info) {
      foreach (array_keys($info) as $bundle) {
        yield [$entityTypeId, $bundle];
      }
    }
  }

  /**
   * Implements hook_local_tasks_alter().
   *
   * @see \entity_editor_tabs_local_tasks_alter()
   */
  public function hookLocalTasksAlter(array &$local_tasks): void {
    foreach ($this->bundles() as [$entity_type_id, $bundle]) {
      $local_task_key = sprintf('entity.%s.export_form', $entity_type_id);
      $local_tasks[$local_task_key] = [
        "route_name" => "entity.$entity_type_id.export_form",
        "title" => "Export",
        "base_route" => "entity.$entity_type_id.canonical",
        "parent_id" => null,
        "weight" => null,
        "options" => [],
        "class" => "Drupal\Core\Menu\LocalTaskDefault",
        "id" => $local_task_key,
        "provider" => "entity_prompt",
      ];
    }
  }

}
