<?php

namespace Drupal\content_entity_sync_ui\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Field UI routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {


      if ($route_name = $entity_type->get('field_ui_base_route')) {
        $entity_route = $collection->get($route_name);


        // Try to get the route from the current collection.
        if (!$entity_route) {
          // TODO: Get canonical route.
          continue;
        }
        $path = $entity_route->getPath();

        $path = $entity_type->getLinkTemplate('edit-form');
        if (empty($path)) {
          continue;
        }

      $options = $entity_route->getOptions();
      if ($bundle_entity_type = $entity_type->getBundleEntityType()) {
        $options['parameters'][$bundle_entity_type] = [
          'type' => 'entity:' . $bundle_entity_type,
        ];
      }



      $options['_admin_route'] = TRUE;

      $route = new Route(
        "$path/export",
        [
          '_form' => '\Drupal\content_entity_sync_ui\Form\EntityExportForm',
//          '_title_callback' => '\Drupal\field_ui\Form\FieldConfigEditForm::getTitle',
          'entity_type_id' => $entity_type_id,
        ],
        ['_permission' => 'export content'],
        $options
      );
      $collection->add("entity.{$entity_type_id}.export_form", $route);
    }
    }
  }

}
