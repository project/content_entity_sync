<?php

declare(strict_types=1);

namespace Drupal\content_entity_sync_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;

/**
 * Provides a Content Entity Sync UI form.
 */
final class EntityExportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'content_entity_sync_ui_entity_export';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $route_parameters = $this->getRouteMatch()->getParameters()->all();
    $entity_type = $route_parameters['entity_type_id'];
    if (empty($entity_type)) {
      return [];
    }

    $entity_id = $route_parameters[$entity_type];
    /** @var \Drupal\content_entity_sync\Services\ContentEntitySerializer $service */
    $service = \Drupal::service('content_entity_sync.entity.serializer');

    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
    $yaml = $service->serialize($entity);

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Entity'),
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#default_value' => trim(Yaml::encode($yaml)),
      '#rows' => 14,
      '#description' => $this->t('File will be saved to %filename', [
        '%filename' => 'asd.txt',
      ]),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'export' => [
        '#type' => 'submit',
        '#value' => $this->t('Export to file'),
      ],
      '#access' => false
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // @todo Validate the form here.
    // Example:
    // @code
    //   if (mb_strlen($form_state->getValue('message')) < 10) {
    //     $form_state->setErrorByName(
    //       'message',
    //       $this->t('Message should be at least 10 characters.'),
    //     );
    //   }
    // @endcode
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->messenger()->addStatus($this->t('The message has been sent.'));
    $form_state->setRedirect('<front>');
  }

}
