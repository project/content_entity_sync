<?php

namespace Drupal\content_entity_sync\Commands;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Site\Settings;
use Drush\Commands\DrushCommands;

class ContentSyncCommands extends DrushCommands {

  public function __construct()
  {
    parent::__construct();

  }

  /**
   * Drush command to export content entities.
   *
   * @param string $entity_type
   *   Entity type to export.
   * @command content_entity_sync:export
   * @aliases con-ex cox
   * @option bundle
   *   Filters entities by the bundle of the entity.
   * @usage content_entity_sync:export --bundle=article --all node
   */
  public function export($entity_type = '', $options = ['bundle' => FALSE]) {
    $existing_content_dir = Settings::get('content_sync_directory');
    if (!is_dir($existing_content_dir)) {
      throw new \Exception(dt('Existing content directory @dir not found', ['@dir' => $existing_content_dir]));
    }

    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);

    // Build the entity query dynamically.
    $entity_query = $storage->getQuery()->accessCheck(FALSE);

    // Filter entities by bundle.
    if ($options['bundle']) {
      $bundles = explode(',', $options['bundle']);
      $entity_query->condition('type', $bundles, 'IN');
    }

    $results = $entity_query->execute();
    foreach ($results as $result) {
      $entity = $storage->load($result);

      $export_values = [
        'uuid' => $entity->uuid(),
        'langcode' => $entity->language()->getId(),
        'type' => $entity->getEntityTypeId(),
        'bundle' => $entity->bundle(),
        'id' => $entity->id(),
      ];

      $export_values['dependencies'] = [
        'config' => [
          sprintf('%s.%s_type.%s', $entity->getEntityTypeId(), $entity->getEntityTypeId(), $entity->bundle()),
        ],
      ];


      /** @var EntityFieldManagerInterface $efm */
$efm = \Drupal::service('entity_field.manager');
  $fields = $efm->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
      $entity_references = [];
      foreach ($fields as $field) {
        if (isset($export_values[$field->getName()])) {
          continue;
        }

        if (!$field->getFieldStorageDefinition()->isBaseField()) {
          $export_values['dependencies']['config'][] = 'field.field.' . $field->getConfig($entity->bundle())->getOriginalId();
        }

        $field_value = $entity->get($field->getName())->getValue();

        foreach ($field_value as $delta => $item) {
          if (empty($item['target_id'])) {
            continue;
          }

          $referenced_entity_type = $field->getFieldStorageDefinition()->getPropertyDefinition('entity')->getConstraint('EntityType');

          $referenced_entity = \Drupal::entityTypeManager()->getStorage($referenced_entity_type)->load($item['target_id']);

          if ($referenced_entity instanceof EntityInterface) {
            $field_value[$delta]['_entity'] = $referenced_entity->getConfigDependencyName();
          }

          $entity_references[] = $referenced_entity->getConfigDependencyName();
        }

        if ($field->getConfig($entity->bundle())->getFieldStorageDefinition()->getCardinality() === 1 && count($field_value)) {
          $field_value = array_shift($field_value);
        }

        if (count($field_value) === 1 && isset($field_value['value'])) {
          $field_value = $field_value['value'];
        }
        $export_values['fields'][$field->getName()] = $field_value;
      }
      $export_values['dependencies']['entity'] = array_unique($entity_references);

      $filename = sprintf('%s.%s.%s.yml', $entity->getEntityTypeId(), $entity->bundle(), $entity->uuid());

      $filepath = $existing_content_dir . DIRECTORY_SEPARATOR . $filename;

      file_put_contents($filepath, Yaml::encode($export_values));











      $this->logger()->success(dt('Exported @name', [
        '@name' => $entity->getConfigDependencyName()
      ]));



    }



    $this->output()->writeln($entity_type);
  }

  /**
   * Drush command to export content entities.
   *
   * @param string $entity_type
   *   Entity type to import.
   * @command content_entity_sync:export
   * @aliases con-im coi
   * @option bundle
   *   Filters entities by the bundle of the entity.
   * @usage content_entity_sync:export --bundle=article --all node
   */
  public function import($entity_type = '', $options = ['bundle' => FALSE]) {
    $existing_content_dir = Settings::get('content_sync_directory');
    if (!is_dir($existing_content_dir)) {
      throw new \Exception(dt('Existing content directory @dir not found', ['@dir' => $existing_content_dir]));
    }

    foreach (scandir($existing_content_dir) as $item) {
      $path = $existing_content_dir . DIRECTORY_SEPARATOR . $item;
      if (!is_file($path)) {
        continue;
      }
      $type = explode('.', $item, 2);
      $type = array_shift($type);
      if (!empty($entity_type) && $type !== $entity_type) {
        continue;
      }

      $content = Yaml::decode(file_get_contents($path));

      $storage = \Drupal::entityTypeManager()->getStorage($content['type']);

      $entity = $storage->load($content['id']);

      if ($entity instanceof FieldableEntityInterface) {
        foreach ($content['fields'] as $name => $value) {
          if (is_array($value)) {
            foreach ($value as $key => $item) {
              if (str_starts_with($key, '_')) {
                unset($value[$key]);
              }
            }
          }
          var_dump($name);
          var_dump($value);
          $entity->set($name, $value);
        }
        $entity->save();
        continue;
      }

      $entity = $storage->create($content['fields']);
      $entity->save();

    }


  }

}
